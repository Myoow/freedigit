import axios from 'axios';

export async function request(
    method,
    url,
    data,
    token = null
) {
    const headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    try {
        const response = await axios({
            method,
            url: process.env.NEXT_PUBLIC_API_URL + '/api' + url,
            data,
            headers,
            responseType: 'json'
        });

        return { response: response.data };
    } catch (error) {
        console.error(error);
        return { error };
    }
}
