import axios from 'axios';

export default async function fetcher(url, token = null) {
    let headers = {};

    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    try {
        const response = await axios(process.env.NEXT_PUBLIC_API_URL + '/api' + url, { headers });

        return response.data;
    } catch (error) {
        console.error(error);
        throw { error };
    }
}
