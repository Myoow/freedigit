import { parseCookies } from "nookies";
import useSWR from "swr";

export default function useUser() {
    const cookies = parseCookies();
    const { data, error } = useSWR(['/user/me', cookies.token || null]);

    return {
        user: data,
        error,
    };
}
