import NextHead from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { destroyCookie, parseCookies } from 'nookies';
import Button from '../Button/Button';
import LinkButton from '../Button/LinkButton';
import styles from './index.module.scss';

function Layout({
    pageTitle = 'FreeDigit',
    children,
}) {
    const router = useRouter();
    const cookies = parseCookies();

    const logout = () => {
        destroyCookie(null, 'role', {path:'/'});
        destroyCookie(null, 'token', {path:'/'});
        router.push("/");
    }

    return (
        <>
            <NextHead>
                <meta charSet="UTF-8" />
                <title>{pageTitle}</title>
            </NextHead>
            <div className={styles.navBar}>
				<Link href='/'>FreeDigit</Link>
                <div className='flex gap-4'>
                    {!!cookies && !cookies.role &&
                        <>
                            <Link href='/register'>Inscription</Link>
                            <Link href='/login'>Connexion</Link>
                        </>
                    }
                    {cookies && cookies.role === "freelancer" &&
                        <>
                            <Link href='/register'>Offres</Link>
                            <Link href='/login'>Vos candidatures</Link>
                        </>
                    }
                </div>
                {cookies && cookies.role &&
                    <div className='flex gap-4'>
                        <LinkButton title="Profile" href="/profile" size="sm"/>
                        <Button
                            title="Deconnexion"
                            onClick={logout}
                            size="sm"
                        />
                    </div>
                }
			</div>
            <div className={styles.container}>
                {children}
            </div>
        </>
    );
}

export default Layout;
