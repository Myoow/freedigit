import Image from 'next/image';
import img1 from '/public/img/6963.jpg';
import img2 from '/public/img/4565.jpg';
import { parseCookies } from 'nookies';

function Home({
}) {
    const cookies = parseCookies();

    return (
        <>
            {!!cookies &&
                <>
                    <h1 className='text-4xl text-center p-8'>Bienvenue sur FreeDigit</h1>
                    <div className='grid grid-cols-2'>
                        <Image
                        src={img1}
                        alt="img1"
                        title="Freelancer"
                        width={800}
                        height={500}
                        placeholder="blur"
                        />
                        <div className='m-24 h-fit rounded-lg flex items-center justify-center bg-gradient-to-r from-sky-500 to-indigo-500 text-gray-200'>
                            <p className='p-8 text-justify'>
                                Grâce à FreeDigit, les freelances peuvent se montrer disponible
                                dans la liste des freelances, afin d'être contacter par des recruteurs pour des missions.
                                Cependant vous pouvez aussi consulter des offres d'emplois créées par les recruteurs eux-mêmes et ainsi
                                y postuler.
                            </p>
                        </div>
                    </div>
                    <div className='grid grid-cols-2'>
                        <div className='m-24 h-fit rounded-lg flex items-center justify-center bg-gradient-to-r from-sky-500 to-indigo-500 text-gray-200'>
                            <p className='p-8 text-justify'>
                                Les recruteurs peuvent consulter la liste des freelances,
                                afin de contacter les freelances qu'ils jugent les plus compétent pour leurs missions.
                                Cependant vous pouvez aussi créer des offres d'emplois visibles par les freelances qui pourront y postuler.
                            </p>
                        </div>
                        <Image
                        src={img2}
                        alt="img2"
                        title="Recruiter"
                        width={800}
                        height={500}
                        placeholder="blur"
                        />
                    </div>
                </>
            }
        </>
    );
}

export default Home;



