import styles from '../index.module.scss';
import classnames from 'classnames';

function Button({
    title,
    onClick,
    buttonType = 'button',
    type = 'main',
    size,
    isLoading = false,
    isSuccess = false,
    disabled = false,
    className,
    ariaLabel,
}) {
    return (
        <button
            type={buttonType}
            className={classnames(styles.btn, styles[type], className, {
                [styles.small]: size === 'sm',
                [styles.large]: size === 'lg',
            })}
            onClick={onClick}
            disabled={disabled || isLoading || isSuccess}
            aria-label={ariaLabel}
        >
            {title && <span>{title}</span>}
        </button>
    );
}

export default Button;
