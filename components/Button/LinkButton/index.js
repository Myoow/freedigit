import styles from '../index.module.scss';
import { memo } from 'react';
import classnames from 'classnames';
import Link from 'next/link';

function LinkButton({
    title,
    href,
    type = 'main',
    size,
    disabled,
    className,
}) {
    return (
        <Link href={href}>
            <a
                className={classnames(styles.btn, styles[type], className, {
                    [styles.small]: size === 'sm',
                    [styles.large]: size === 'lg',
                    'pointer-events-none': disabled,
                })}
            >
                {title && <span>{title}</span>}
            </a>
        </Link>
    );
}

export default memo(LinkButton);
