import { setCookie } from 'nookies';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { request } from '../libs/request';
import jwt_decode from 'jwt-decode';
import Layout from '../components/Layout';
import Button from '../components/Button/Button';
import { useRouter } from 'next/router';

function Register() {
  const router = useRouter();
  const [state, setState] = useState({
    isLoading: false,
    response: null,
    error: null
  });
  const { isLoading, response, error } = state;
  const { register, handleSubmit, formState: { errors } } = useForm();

  const onSubmit = async (data) => {
    setState((current) => ({
      ...current,
      isLoading: true
    }))
    const { response, error } = await request('POST', '/register/', data);
    setState({
      isLoading: false,
      response,
      error,
    });
  }

  useEffect(() => {
    if (!response) {
      return;
    }

    setCookie(null, 'token', response.token, {
      maxAge: 14400, // 4 hours
      path: '/',
    });

    const decodedToken = jwt_decode(response.token);
    setCookie(null, 'role', decodedToken.roles.includes('ROLE_RECRUITER') ? 'recruiter' : 'freelancer', {
      maxAge: 14400, // 4 hours
      path: '/',
    });

    router.push('/');
  },
  [response, router]);

  return (
    <Layout>
    <form onSubmit={handleSubmit(onSubmit)}>
        <div className="formContainer">
            <span>INSCRIPTION</span>
            <input {...register("name", { required: true })} placeholder="Nom"/>
            <input {...register("firstname", { required: true })} placeholder="Prénom"/>
            <input {...register("email", { required: true })} placeholder="Email"/>
            <input {...register("password", { required: true })} placeholder="Mot de passe" type="password"/>
            <select {...register("roles.0")}>
                <option value="ROLE_FREELANCER">Freelance</option>
                <option value="ROLE_RECRUITER">Recruteur</option>
            </select>
            {errors.exampleRequired && <span>Champs requis</span>}
            <Button buttonType="submit" title={isLoading ? "Inscription..." : "S'inscrire"} isLoading={isLoading}/>
        </div>
    </form>
    </Layout>
  )
}

export default Register;
