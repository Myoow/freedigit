import '../styles/globals.scss';
import '../styles/app.scss';
import fetcher from '../libs/fetcher';
import { SWRConfig } from 'swr';

function MyApp({ Component, pageProps }) {
  return <SWRConfig
    value={{
        fetcher,
        onError: (error) => {
            console.error(error.errorResponse || 'Error');
        },
    }}
  >
    <Component {...pageProps} />
  </SWRConfig>
}

export default MyApp
